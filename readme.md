
## 참고 사이트
* https://blog.outsider.ne.kr/1241 - apex for lambda
* https://blog.outsider.ne.kr/1287 - apex for api g

## 특이사항
* 람다 함수 생성 후 로컬에서 API Gateway 설정은 별도로 해 줘야 함
* apex를 수동으로 서버에서 설치 후 export 명령으로 배포서버에 AWS Credential을 최초 한번 설정해야 ...
* project.json 파일에서 실행환경 설정함
* IAM 젠킨스 Role에 Lambda, API G Full 권한을 줘야 함.

## 명령어
  * 람다 배포 명령어
      * apex deploy
  * API G 배포
      * 루트 폴더 아래에 infrastructure 폴더 생성 후 테라폼 파일 생성. 
       해당 폴더에서 terraform init도 해 줘야 함
      * 실제 인프라(테라폼) 배포 명령어는 루트 폴더에서 실행 
        * apex infra apply    
  * 삭제 명령어
      * API G 삭제 (루트 폴더에서 실행)
        * apex infra destroy
      * 람다함수 삭제
        * apex delete
        
## 소스코드 설명
  * /sh/install.apex.sh
      * apex 설치 및 환경변수 설정함     
          
