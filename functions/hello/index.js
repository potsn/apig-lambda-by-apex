
/*
const request = require('request');

console.log('starting function');

const ping = (url, cb) => {
  if (!url) { return cb(new Error('[BadRequest] URL required')); }

  request(url, (err, response, body) => {
    console.log('ping statusCode:', response && response.statusCode);
    console.log('ping body:', body);
    cb(err, body);
  });
};

exports.handle = (event, ctx, cb) => {
  console.log('processing event: %j', event);

  ping(event.url, (err, result) => {
    if (err) {
      const error = {
        message: err.message,
        stackTrace: err.stack,
        event: event,
        requestId : ctx.awsRequestId,
      }
      if (!err.message.match(/BadRequest/)) {
        error.message = `[InternalServerError] ${err.message}`
      }
      console.log('err: %j', error);
      return cb(JSON.stringify(error));
    }

    console.log('result: %j', result);
    return cb(null, result);
  });
};
*/


console.log('starting function')
exports.handle = function(e, ctx, cb) {
  console.log('processing event: %j', e)
  return cb(null, { hello: 'apex lambda world ~' })
}
