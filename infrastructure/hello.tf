// infrastructure/hello.tf

// API 정의
resource "aws_api_gateway_rest_api" "status_api" {
  name        = "StatusAPI"
  description = "상태를 조회하는 API"
}

// API 리소스 정의 (/check 같은 경로)
resource "aws_api_gateway_resource" "status_api_resource_check" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  parent_id   = "${aws_api_gateway_rest_api.status_api.root_resource_id}"
  path_part   = "check"
}

// API 리소스의 메서드 정의 (GET, POST 등)
resource "aws_api_gateway_method" "status_api_resource_check_post" {
  rest_api_id   = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id   = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method   = "POST"
  authorization = "NONE"
}

// API와 Lambda 통합
resource "aws_api_gateway_integration" "status_api_resource_check_post" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method = "${aws_api_gateway_method.status_api_resource_check_post.http_method}"
  type = "AWS"
  integration_http_method = "POST"
  uri = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${var.apex_function_hello}/invocations"
}

// API 배포
resource "aws_api_gateway_deployment" "status_api_test" {
  depends_on = ["aws_api_gateway_integration.status_api_resource_check_post"]
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  stage_name  = "test"
  stage_description = "${timestamp()}"
  description = "Deployed at ${timestamp()}"
}

// Lambda에 호출할 권리
resource "aws_lambda_permission" "status_api_resource_check_post" {
  statement_id = "AllowInvokeFromAPIGateway"
  action = "lambda:InvokeFunction"
  function_name = "${var.apex_function_hello_name}"
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_deployment.status_api_test.execution_arn}/${aws_api_gateway_integration.status_api_resource_check_post.integration_http_method}${aws_api_gateway_resource.status_api_resource_check.path}"
}


// API 메서드 설정
resource "aws_api_gateway_method_settings" "status_api_resource_check_post" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  stage_name  = "${aws_api_gateway_deployment.status_api_test.stage_name}"
  method_path = "${aws_api_gateway_resource.status_api_resource_check.path_part}/${aws_api_gateway_method.status_api_resource_check_post.http_method}"

  settings {
    metrics_enabled = true
    logging_level = "INFO"
  }
}

// 200 응답 매핑
resource "aws_api_gateway_method_response" "status_api_resource_check_post_200" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method = "${aws_api_gateway_method.status_api_resource_check_post.http_method}"
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "status_api_resource_check_post_200" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method = "${aws_api_gateway_method.status_api_resource_check_post.http_method}"
  status_code = "${aws_api_gateway_method_response.status_api_resource_check_post_200.status_code}"

  response_templates = {
    "application/json" = ""
  }
}


// 400 응답 매핑
resource "aws_api_gateway_method_response" "status_api_resource_check_post_400" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method = "${aws_api_gateway_method.status_api_resource_check_post.http_method}"
  status_code = "400"
  response_models = {
    "application/json" = "Error"
  }
}

resource "aws_api_gateway_integration_response" "status_api_resource_check_post_400" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method = "${aws_api_gateway_method.status_api_resource_check_post.http_method}"
  status_code = "${aws_api_gateway_method_response.status_api_resource_check_post_400.status_code}"
  selection_pattern = ".*BadRequest.*"
  response_templates = {
    "application/json" = <<EOF
#set ($errorMessageObj = $util.parseJson($input.path('$.errorMessage')))

{
  "message": "$errorMessageObj.message",
  "stack": "$errorMessageObj.stackTrace",
  "requestId": "$errorMessageObj.requestId"
}
EOF
  }
}

// 500 응답 매핑
resource "aws_api_gateway_method_response" "status_api_resource_check_post_500" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method = "${aws_api_gateway_method.status_api_resource_check_post.http_method}"
  status_code = "500"
  response_models = {
    "application/json" = "Error"
  }
}

resource "aws_api_gateway_integration_response" "status_api_resource_check_post_500" {
  rest_api_id = "${aws_api_gateway_rest_api.status_api.id}"
  resource_id = "${aws_api_gateway_resource.status_api_resource_check.id}"
  http_method = "${aws_api_gateway_method.status_api_resource_check_post.http_method}"
  status_code = "${aws_api_gateway_method_response.status_api_resource_check_post_500.status_code}"
  selection_pattern = ".*InternalServerError.*"
  response_templates = {
    "application/json" = <<EOF
#set ($errorMessageObj = $util.parseJson($input.path('$.errorMessage')))

{
  "message": "$errorMessageObj.message",
  "stack": "$errorMessageObj.stackTrace",
  "requestId": "$errorMessageObj.requestId"
}
EOF
  }
}
