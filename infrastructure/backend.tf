terraform {
  backend "s3" {
    bucket = "tf-state-study"
    key    = "lambda-apig-by-apex.terraform.tfstate"
    region = "eu-west-1"
  }
}
