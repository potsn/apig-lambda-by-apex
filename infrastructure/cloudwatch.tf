
// infrastructure/cloudwatch.tf

// cloudwatch를 사용할 policy
data "aws_iam_policy_document" "allow_log_to_cloudwatch" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents"
    ]
    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "allow_log_to_cloudwatch" {
  name   = "allow_log_to_cloudwatch"
  role = "${aws_iam_role.api_gateway_cloudwatch.id}"
  policy = "${data.aws_iam_policy_document.allow_log_to_cloudwatch.json}"
}

// cloudwatch를 사용할 role
data "aws_iam_policy_document" "allow_log_to_cloudwatch_from_apigateway" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "api_gateway_cloudwatch" {
  name = "api_gateway_cloudwatch"
  assume_role_policy = "${data.aws_iam_policy_document.allow_log_to_cloudwatch_from_apigateway.json}"
}

// 로깅을 위한 CloudWatch 설정
resource "aws_api_gateway_account" "api_gateway_account" {
  cloudwatch_role_arn = "${aws_iam_role.api_gateway_cloudwatch.arn}"
}
